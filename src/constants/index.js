export const API = "https://api.dev.pastorsline.com/api/contacts.json"
export const TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE3MSwiZXhwIjoxNjM5NDY2NjE1fQ.9vE-glLQtV2NT3gNMkqeRkrWWZAhYCqX-_ibs7lC8GY"

export const DEFAULT_QUERY_PARAMS = {
  COMPANY_ID: 171,
  US_ID: 226
}

export const CONTACT_TYPE = {
  ALL: "ALL",
  US: "US",
}
