import React from "react"
import { Provider } from "react-redux"
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom"
import { CONTACT_TYPE } from "./constants"
import Contacts from "./modules/containers/contacts/Contacts"
import Dashboard from "./modules/containers/dashboard/Dashboard"
import { ROUTES } from "./modules/services/routes"
import { store } from "./modules/store/store"

function App() {

  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route
            path={ROUTES.BASE}
            exact
            render={props => <Dashboard {...props} />}
          />
          <Route
            exact
            path={[ROUTES.CONTACTS, `${ROUTES.CONTACTS}/:countryId`]}
            render={props => <Contacts {...props} type={CONTACT_TYPE.ALL} />}
          />
          <Redirect to={ROUTES.BASE} />
        </Switch>
      </Router>
    </Provider>
  )
}

export default App
