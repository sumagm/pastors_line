import axios from 'axios'
import {TOKEN} from '../../../constants'

export const registerInterceptors = () => {
	axios.interceptors.request.use(
    (config) => {
      config.headers = {
        Authorization: `Bearer ${TOKEN}`
      };
      return config;
    },
    (err) => {
      return Promise.reject(err);
    }
  );
}

