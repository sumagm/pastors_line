import { getContacts } from "./contacts"

const API = {
  contact: {
    getContacts,
  },
}
export default API
