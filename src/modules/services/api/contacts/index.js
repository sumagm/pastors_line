import { API, DEFAULT_QUERY_PARAMS } from "../../../../constants"
import axios from 'axios'

export const getContacts = async (
  pageNumber = 0,
  searchQuery,
  countryId = null,
  companyId = DEFAULT_QUERY_PARAMS.COMPANY_ID
) => {
  let path = `${API}?companyId=${companyId}&page=${pageNumber}`
  if (searchQuery) path = path + `&query=${searchQuery}`
  if (countryId) path = path + `&countryId=${countryId}`
  const response = await axios.get(path)
  return response.data
}