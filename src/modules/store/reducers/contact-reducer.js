import { createSelector } from "reselect"
import { filterByFirstName } from "../../../util/util"
import API from "../../services/api"

export const actions = {
  FETCH_CONTACT_START: "FETCH_CONTACT_START",
  FETCH_CONTACT_SUCCESS: "FETCH_CONTACT_SUCCESS",
  FETCH_MORE_STARTED: "FETCH_MORE_STARTED",
  FETCH_MORE_SUCCESS: "FETCH_MORE_SUCCESS",
  TOGGLE_ONLY_EVEN: "TOGGLE_ONLY_EVEN",
  QUERY_CHANGED: "QUERY_CHANGED",
  QUERY_CHANGE_STARTED: "QUERY_CHANGE_STARTED",
  FETCH_CONTACT_BY_ID: "FETCH_CONTACT_BY_ID",
}

const initialState = {
  contact: {
    total: 0,
    contacts_ids: [],
    contacts: [],
    contact: {},
    searchContacts: {},
  },
  currentPage: 0,
  onlyEven: false,
  loading: true,
  fetchMoreLoading: true,
  queryText: "",
}

export const contactReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_CONTACT_START:
      return {
        ...state,
        loading: true,
      }

    case actions.FETCH_CONTACT_SUCCESS: {
      return {
        ...state,
        contact: {
          ...action.payload.data,
          contacts: [...Object.values(action.payload.data.contacts)],
        },
        loading: false,
      }
    }

    case actions.FETCH_MORE_STARTED:
      return {
        ...state,
        fetchMoreLoading: true,
      }

    case actions.FETCH_MORE_SUCCESS:
      return {
        ...state,
        fetchMoreLoading: false,
        currentPage: state.currentPage + 1,
        contact: {
          ...state.contact,
          total: action.payload.data.total,
          contacts_ids: [
            ...state.contact.contacts_ids,
            ...action.payload.data.contacts_ids,
          ],
          contacts: [
            ...state.contact.contacts,
            ...Object.values(action.payload.data.contacts),
          ],
        },
      }

    case actions.TOGGLE_ONLY_EVEN:
      return {
        ...state,
        onlyEven: !state.onlyEven,
      }

    case actions.QUERY_CHANGE_STARTED:
      return {
        ...state,
        loading: true,
        fetchMoreLoading: false,
        queryText: action.payload.query,
        contact: {
          ...state.contact,
          searchContacts: {},
        },
      }

    case actions.QUERY_CHANGED:
      return {
        ...state,
        loading: false,
        queryText: action.payload.query,
        contact: {
          ...state.contact,
          searchContacts: filterByFirstName(
            state.contact.contacts,
            action.payload.query
          ),
        },
      }

    case actions.FETCH_CONTACT_BY_ID:
      return {
        ...state,
        contact: {
          ...state.contact,
          contact: { ...state.contact.contacts[action.id] },
        },
      }

    default:
      return { ...state }
  }
}

/**
 * selectors
 */
export const contactSelector = createSelector(
  store => {
    if (store.contacts.queryText === "")
      return store.contacts.contact.contacts
    return store.contacts.contact.searchContacts
  },
  contacts => contacts
)

export const loadingSelector = store => store.contacts.loading 

export const hasMoreSelector = store =>
  store.contacts.contact.total >
  store.contacts.contact.contacts_ids.length

export const currentPageSelector = store => store.contacts.currentPage

/**
 * Thunk action creators
 */
export const fetchMoreContacts = (
  currentPage,
  searchText,
  countryId
) => async dispatch => {
  dispatch({ type: actions.FETCH_MORE_STARTED })
  const data = await API.contact.getContacts(currentPage + 1, searchText, countryId)
  dispatch({ type: actions.FETCH_MORE_SUCCESS, payload: { data } })
}

export const fetchContacts = (pageNumber, searchText, countryId) => async dispatch => {
  if (pageNumber !== 0)
    return dispatch(fetchMoreContacts(pageNumber, searchText, countryId))
  dispatch({ type: actions.FETCH_CONTACT_START })
  const data = await API.contact.getContacts(pageNumber, searchText, countryId)
  dispatch({ type: actions.FETCH_CONTACT_SUCCESS, payload: { data } })
}
