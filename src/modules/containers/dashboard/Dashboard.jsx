import React from "react"
import "./styles/dashboard.scss"
import { ROUTES } from "../../services/routes"
import { Button } from "../../components"
import {DEFAULT_QUERY_PARAMS} from "../../../constants"

const Dashboard = props => {
  return (
    <div className='dashboard-container'>
      <div className='btn-container'>
        <Button
          className='button'
          onClick={() => props.history.push(ROUTES.CONTACTS)}
          variant='primary'
        >
          All Contacts
        </Button>
        <Button
          className='button'
          onClick={() => props.history.push(`${ROUTES.CONTACTS}/${DEFAULT_QUERY_PARAMS.US_ID}`)}
          variant='secondary'
        >
          US contacts
        </Button>
      </div>
    </div>
  )
}

export default Dashboard
