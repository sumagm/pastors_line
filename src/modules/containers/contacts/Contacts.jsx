import React, { useCallback, useEffect, useMemo, useState } from "react"
import InfiniteScroll from "react-infinite-scroll-component"
import { useDispatch, useSelector } from "react-redux"
import { DEFAULT_QUERY_PARAMS } from "../../../constants"
import { Button, Checkbox, Modal, SearchBox, Spinner, ContactDetails } from "../../components"
import { ROUTES } from "../../services/routes"
import {
  contactSelector,
  currentPageSelector,
  fetchContacts,
  hasMoreSelector,
  loadingSelector,
} from "../../store/reducers/contact-reducer"
import "./style/contacts.scss"
import * as util from '../../../util/util'

const contactButtonLabel = {
  all: "All Contact",
  us: "US Contact",
}

export default function Contacts(props) {
  const contacts = useSelector(contactSelector)
  const loading = useSelector(loadingSelector)
  const hasMore = useSelector(hasMoreSelector)
  const currentPage = useSelector(currentPageSelector)
  const [typingTimeout, setTypingTimeout] = useState(0)
  const [searchText, setSearchText] = useState("")
  const [showContactDetailPopup, setShowContactDetailPopup] = useState(false)
  const [contact, setContact] = useState(null)
  const [isOnlyEven, setIsOnlyEven] = useState(false)
  const dispatch = useDispatch()

  const getContacts = useCallback(
    (page, searchQuery, countryId) => {
      dispatch(fetchContacts(page, searchQuery, countryId))
    },
    [dispatch]
  )

  const getValuesFromParam = useCallback(
    key => {
      const { match: {params} } = props
      return params.hasOwnProperty(key) && params[key]
    },
    [props]
  )

  const countryId = useMemo(() => getValuesFromParam("countryId"), [getValuesFromParam])

  useEffect(() => {
    getContacts(0, "", countryId)
  }, [getContacts, countryId])

  const fetchMoreContact = () => {
    if (hasMore) {
      getContacts(currentPage + 1, searchText, countryId)
    }
  }

  const onSearchTextChange = e => {
    if (typingTimeout) {
      clearTimeout(typingTimeout)
    }
    setSearchText(e.target.value)
    setTypingTimeout(
      setTimeout(() => getContacts(0, searchText, countryId), 500)
    )
  }

  const onSearchSubmit = e => {
    if (typingTimeout) {
      clearTimeout(typingTimeout)
    }
    getContacts(0, searchText, countryId)
  }

  const onlyEvenToggle = () => {
    setIsOnlyEven(onlyEven => !onlyEven)
  }

  const onClose = () => {
    props.history.push(ROUTES.BASE)
  }

  const setContactDataViewState = () => {
    setShowContactDetailPopup(!showContactDetailPopup)
  }

  const setContactDataView = contact => {
    setContactDataViewState()
    setContact(contact)
  }

  // if no country id means All contact
  const accentColor = !!countryId ? "secondary" : "primary"

  const getTableHeading = () => {
    return (
      <div className={`contact-list-heading ${accentColor}`}>
        <div className="contact-first-name">First Name</div>
        <div className="contact-last-name">Last Name</div>
        <div className="contact-email">Email</div>
        <div className="contact-phone">Phone</div>
      </div>
    )
  }

  const filteredContactList = isOnlyEven
    ? contacts.filter(contact => contact.id % 2 === 0)
    : contacts

  const getTableData = () => {
    return filteredContactList.map(contact => {
      return (
        <>
          <div
            className="contact-list-heading table-data"
            key={contact.id}
            onClick={() => setContactDataView(contact)}
          >
            <div className="contact-first-name">{util.getValue(contact.first_name)}</div>
            <div className="contact-last-name">{util.getValue(contact.last_name)}</div>
            <div className="contact-email"> {util.getValue(contact.email)} </div>
            <div className="contact-phone">{util.getValue(contact.phone_number)}</div>
          </div>
          <hr />
        </>
      )
    })
  }

  const getHeader = () => {
    const isAllCountry = !!!countryId
    return (
      <div className="d-flex justify-content-between">
        <Button
          className="mr-3"
          variant={isAllCountry ? "primary" : "link"}
          disabled={isAllCountry}
          onClick={() => props.history.push(ROUTES.CONTACTS)}
        >
          {contactButtonLabel.all}
        </Button>
        <Button
          variant={isAllCountry ? "link" : "secondary"}
          disabled={!isAllCountry}
          onClick={() =>
            props.history.push(
              `${ROUTES.CONTACTS}/${DEFAULT_QUERY_PARAMS.US_ID}`
            )
          }
        >
          {contactButtonLabel.us}
        </Button>
        <div className="ml-auto">
          <SearchBox
            value={searchText}
            onChange={event => onSearchTextChange(event)}
            onSubmit={event => onSearchSubmit(event)}
          />
        </div>
      </div>
    )
  }

  const getFooter = () => (
    <>
      <Checkbox
        label="Only even"
        checked={isOnlyEven}
        onChange={onlyEvenToggle}
      />
      <Button
        className="ml-auto"
        variant="default"
        color={accentColor}
        onClick={onClose}
      >
        Close
      </Button>
    </>
  )

  return (
    <>
      <Modal show={true}>
        <Modal.Header>{getHeader()}</Modal.Header>
        <Modal.Content isLoading={loading} loaderColor={accentColor} fullHeight>
          {!filteredContactList.length ? (
            <div className="no-contacts-msg">No Contacts Found</div>
          ) : (
            <>
              {getTableHeading()}
              <InfiniteScroll
                dataLength={filteredContactList.length}
                next={fetchMoreContact}
                hasMore={hasMore}
                loader={<Spinner variant="ellipsis" color={accentColor} />}
                height={489}
                endMessage={
                  <p style={{ textAlign: "center" }}>
                    <b>You have seen it all</b>
                  </p>
                }
              >
                {getTableData()}
              </InfiniteScroll>
            </>
          )}
        </Modal.Content>
        <Modal.Footer>{getFooter()}</Modal.Footer>
      </Modal>
      {showContactDetailPopup && (
        <ContactDetails
          accent={accentColor}
          onClose={setContactDataViewState}
          contact={contact}
        />
      )}
    </>
  )
}
