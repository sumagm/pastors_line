import React, { useRef, useEffect } from 'react'
import {Scrollbars} from 'react-custom-scrollbars'

const CustomScrollbar = (props) => {
  return (
    <Scrollbars
      style={{height: "calc(100vh - 270px)", width:"100%",overflow:"hidden"}}
      onScrollFrame={(values)=> props.onScroll(values)}
    >
      {props.children}  
    </Scrollbars>
  )
}

export default CustomScrollbar