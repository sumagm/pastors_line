import React from "react"
import { Button } from ".."
import Modal from "../modal/Modal"
import "./style/contact-details.scss"

const dataLabels = {
  first_name: "First Name",
  last_name: "Last Name",
  email: "Email",
  phone_number: "Phone",
  country_id: "Country ID",
}

const ContactDetails = ({ contact, accent, ...props }) => {
  const getContactDetails = () => {
    return Object.keys(contact)
      .filter(
        keys =>
          keys === "first_name" ||
          keys === "last_name" ||
          keys === "phone_number" ||
          keys === "country_id"
      )
      .map(key => {
        if (key === "id") return null
        return (
          <div className="contact-data-field">
            <span className="contact-data-field-label data-field">
              {dataLabels[key]}
            </span>
            <span className="field-delimiter" />
            <span className="contact-data-field-value data-field">
              {contact[key]}
            </span>
          </div>
        )
      })
  }

  return (
    <Modal size="md" onClose={props.onClose}>
      <Modal.Header>Contact Details</Modal.Header>
      <Modal.Content>{getContactDetails()}</Modal.Content>
      <Modal.Footer>
        <Button className="ml-auto" variant="default" color={accent} onClick={props.onClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ContactDetails
