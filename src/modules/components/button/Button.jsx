import React from "react"
import "./styles/button.scss"

const btnType = {
  PRIMARY: "primary",
  SECONDARY: "secondary",
  DEFAULT: "default",
}

const Button = ({
  variant = btnType.PRIMARY,
  onClick,
  disabled,
  color,
  className,
  ...props
}) => {
  return (
    <div className={className}>
      <button
        className={`btn btn-${variant} ${color}`}
        onClick={onClick}
        disabled={disabled}
        {...props}
      >
        {props.children}
      </button>
    </div>
  )
}

export default Button
