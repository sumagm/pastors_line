import React from "react"

const Checkbox = props => {
  const { checked, onChange, id, disabled = false, ...rest } = props
  const randomId = id ? id : Math.random().toString()
  return (
    <div className='form-check d-flex align-items-center'>
      <input
        className='form-check-input'
        type='checkbox'
        id={randomId}
        checked={checked}
        onChange={onChange}
        disabled={disabled}
        style={{ cursor: "pointer" }}
        {...rest}
      />
      <label
        className='form-check-label'
        htmlFor={randomId}
        style={{ cursor: "pointer" }}
      >
        {props.label}
      </label>
    </div>
  )
}

export default Checkbox
