import React from "react"
import { Spinner } from ".."
import "./styles/modal.scss"

const Modal = ({
  handleClose,
  show = true,
  disableBackdrop = false,
  children,
  size = "lg",
  onClose,
}) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none"
  return (
    <div
      className={showHideClassName}
      style={{  background: disableBackdrop && "none"  }}
      onClick={onClose}
    >
      <div className={`modal-dialog ${size}`}>
        <div className='modal-content'>{children}</div>
      </div>
    </div>
  )
}

const Header = ({ children }) => {
  return (
    <div className='modal-header'>
      <h5 className='modal-title w-100'>{children}</h5>
    </div>
  )
}

const Content = ({ children, isLoading, loaderColor, fullHeight }) => {
  const style = fullHeight
    ? { height: "calc(100vh - 196px)" }
    : { maxHeight: "calc(100vh - 196px)" }
  return (
    <div className="modal-body overflow" style={style}>
      {isLoading ? (
        <span className="loader-container">
          <Spinner variant="circle" color={loaderColor} />
        </span>
      ) : children}
    </div>
  )
}

const Footer = ({ children }) => {
  return <div className='modal-footer justify-content-start'>{children}</div>
}

Modal.Header = Header
Modal.Content = Content
Modal.Footer = Footer
export default Modal
