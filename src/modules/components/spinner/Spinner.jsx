import React from "react"
import "./styles/spinner.scss"

const spinnerVariant = {
  circle: "circle",
  ellipsis: "ellipsis",
}

export default function Spinner({
  variant = spinnerVariant.circle,
  className,
  style,
  color = "primary",
  ...props
}) {
  const ellipsis = (
    <div className='lds-ellipsis'>
      {Array.from({ length: 4 }).map((_, i) => (
        <div key={i} className={`bg-color-${color}`}></div>
      ))}
    </div>
  )

  const circle = (
    <div className='lds-default'>
      {Array.from({ length: 12 }).map((_, i) => (
        <div key={i} className={`bg-color-${color}`}></div>
      ))}
    </div>
  )
  return (
    <div className='spinner-container'>
      {variant === spinnerVariant.circle ? circle : ellipsis}
    </div>
  )
}
