import React from "react"
import "./style/search-box.scss"

const SearchBox = props => {
  const { value, onChange, onSubmit, className, ...rest } = props
  const onSubmitHandler = e => {
    e.preventDefault()
    if (onSubmit) onSubmit(e)
  }
  return (
    <form onSubmit={onSubmitHandler} className={className}>
      <input
        type='text'
        className='form-control expansion-control'
        placeholder='Search...'
        value={value}
        onChange={onChange}
        {...rest}
      />
    </form>
  )
}

export default SearchBox
