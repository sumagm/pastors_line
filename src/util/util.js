import _ from "lodash"

export const filterByFirstName = (contacts, inputValue) => {
  const re = new RegExp(_.escapeRegExp(inputValue), "i");
  return contacts.filter(contact => {
    return re.test(contact.first_name)
  })
}

export const getValue = (value) => {
  return value ? value : "---"
}
